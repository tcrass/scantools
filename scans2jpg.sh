#!/bin/bash


adaptiveSharpen=0 #a
blackPoint=0 #b
deviceProfile= #d
ext=tif #e
flatten= #f
resizeDims= #i
localContrSigma=25 #l
localContrAmount=0 #L
keepMess= #m
noClipping= #n
noChannelClipping= #N
useOldTmpFiles= #o
refDir=. #p
quality=95 #q
srgbProfile=/usr/share/color/icc/sRGB_IEC61966-2-1_black_scaled.icc #r
sigmSlope=0 #s
targetDir=. #t
targetSuffix=. #u
vividness=0 #v
whitePoint=100 #w


function printUsage() {
  cat << EOT
scans2jpg - Recursively convert scanned TIFs to JPG images
-----------------------------------------------------------------------
Usage: scans2jpg.sh [options] <path/to/img/or/dir>...
with possible options:
-a <sigma>   : Apply adaptive sharpening with <sigma>.
-b <percent> : Black point. [$blackPoint]
-d <path>    : Path to device profile. If not specified, the source
               images are expected to have a device profile embedded.
-e <ext>     : File extension of source images. [$ext]
-f           : Flatten source directory structure, i.e. don't mirror
               the source directory layout within the target directry.
-i <dim>     : Dimensions to which to scale scans (prior to applying
               adaptive sharpening, if any).
-l <sigma>   : Sigma for local contrast enhancement. [$localContrSigma]
-L <val>     : Amount of local contrast enhancement. [$localContrAmount]
-m           : Don't clean up, i.e. leave the mess of temporary files
               (e.g. for later use with -o).
-n           : When specifying -b and/or -w, prevent clipping.
-o             Re-use old temporary files left over from a previuos
               run with -m.
-p <path>    : Path relative to which the source images will get
               mirrored into the target directory specified by -t.
               [$refDir]
-q <percent> : JPG quality. Larger numbers yield better image quality
               but also lead to increased file size. [$quality]
-r <path>    : Path to sRGB profile. [$srgbProfile]
-s <slope>   : Slope factor for sigmoidal contrast enhancement. [$sigmSlope]
-t <path>    : Path to target directory inside which the directory
               structure the source images are found in will be
               mirrored. [$targetDir]
-u <str>     : Additional path segment(s) to be appended to each
               source path when mirrored inside the target directory.
-v <percent> : Amount by which the images vividness should be increased
               ("digital velvia effect"). [$vividness]
-w <percent> : White point. [$whitePoint]
Requires imagemagick and argyll to be installed and in PATH.
EOT
}

while getopts "a: b: d: e: f i: l: L: m n o p: q: r: s: t: u: v: w: ?" option; do
  case $option in
    a)  adaptiveSharpen=${OPTARG};;
    b)  blackPoint=${OPTARG};;
    d)  deviceProfile=${OPTARG};;
    e)  ext=${OPTARG};;
    f)  flatten=1;;
    l)  localContrSigma=${OPTARG};;
    L)  localContrAmount=${OPTARG};;
    m)  keepMess=1;;
    n)  noClipping=1;;
    o)  useOldTmpFiles=1;;
    p)  refDir=${OPTARG};;
    q)  quality=${OPTARG};;
    r)  srgbProfile=${OPTARG};;
    s)  sigmSlope=${OPTARG};;
    t)  targetDir=${OPTARG};;
    u)  targetSuffix=${OPTARG};;
    v)  vividness=${OPTARG};;
    w)  whitePoint=${OPTARG};;
    \?) printUsage
        exit 0;;
    * ) printUsage
        exit 1;;
  esac
done

shift $((OPTIND-1))
OPTIND=1

if [[ $# -ne 1 ]]; then printUsage; exit 1; fi

refDir=$(readlink -f $refDir)


#--- Process files ----------------------------------------------------

for arg in "$@"; do

    find "$arg" -type f -name "*.$ext" -print0 | sort -z | while IFS= read -r -d '' path; do

        path=$(readlink -f "$path");
        name=$(basename "$path" .$ext)
        tmpName="$name-tmp.tif"

        dir=$(dirname "$path")
        rel=$(realpath --relative-to="$refDir" "$dir")
        if [ "$flatten" ]; then
            destDir="$targetDir/$targetSuffix"
        else
            destDir="$targetDir/$rel/$targetSuffix"
        fi
        destPath="$destDir/$name.jpg"

        echo Processing $path =\> $destPath...

        # Ensure target dir exists
        if [ ! -e "$destDir" ]; then
            mkdir -p "$destDir"
        fi

        # Apply profiles, if necessary
        if [ ! "$useOldTmpFiles" ] || [ ! -e $tmpName ] ; then
            echo "  Applying device profile..."
            if [ "$deviceProfile" ]; then
                cctiff -e "$srgbProfile" -p -ip "$deviceProfile" -ip "$srgbProfile" "$path" "$tmpName"
            else
                cctiff -e "$srgbProfile" -p -ip "$path" -ip "$srgbProfile" "$path" "$tmpName"
            fi
        fi

        # --- Build IM convert command line ---------------------------------

        args=( "$tmpName" )

        # Determine if processing in Lab color space is required
        usingLab=
        if [[ $(echo "$blackPoint > 0" | bc) -eq 1 ]] || \
            [[ $(echo "$whitePoint < 100" | bc) -eq 1 ]] || \
            [[ $(echo "$localContrAmount > 0" | bc) -eq 1 ]] || \
            [[ $(echo "$sigmSlope > 0" | bc) -eq 1 ]]; then
            usingLab=1
            args+=( -colorspace LAB -channel R )
        fi

        if [[ $(echo "$blackPoint > 0" | bc) -eq 1 ]] || [[ $(echo "$whitePoint < 100" | bc) -eq 1 ]]; then

            # Determine blackPoint and whitePoint of converted tiff

            blackLevel=$blackPoint
            whiteLevel=$whitePoint

            # Build -level option, ensuring that no clipping occurs, if desired
            if [ "$noClipping" ]; then
                echo "  Determining black and white level in order to prevent clipping..."
                IFS=, read minL maxL<<EOF
                    $(identify -format '%[fx:100*minima.r],%[fx:100*maxima.r]' -colorspace LAB -channel R "$tmpName")
EOF
                if [[ $(echo "$minL < $blackPoint" | bc) -eq 1 ]]; then
                    blackLevel=$minL
                fi

                if [[ $(echo "$maxL > $whitePoint" | bc) -eq 1 ]]; then
                    whiteLevel=$maxL
                fi
            fi

            args+=( -level $blackLevel%,$whiteLevel% )
        fi

        # Apply unsharp mask for local contrast enhancement
        if [[ $(echo "$localContrAmount > 0" | bc) -eq 1 ]]; then
            args+=( -unsharp 0x$localContrSigma+$localContrAmount+0 )
        fi

        # Apply sigmoidal contrast enhancement
        if [[ $(echo "$sigmSlope > 0" | bc) -eq 1 ]]; then
            args+=( -sigmoidal-contrast ${sigmSlope}x50% )
        fi

        # Return from Lab to sRGB
        if [ "$usingLab" ]; then
            args+=( +channel -colorspace sRGB )
        fi

        # Apply "digital velvia" effect
        if [[ $( echo "$vividness > 0" | bc) -eq 1 ]]; then
            mInc=$(echo "scale=2; 1 + 2*$vividness/100" | bc)
            mDec=$(echo "scale=2; -$vividness/100" | bc)
            args+=( -color-matrix "$mInc $mDec $mDec $mDec $mInc $mDec $mDec $mDec $mInc" )
        fi

        # resize
        if [ "$resizeDims" ]; then
            args+=( -resize $resizeDims )
        fi

        # Apply adaptive sharpening
        if [[ $( echo "$adaptiveSharpen > 0" | bc) -eq 1 ]]; then
            args+=( -adaptive-sharpen 0x$adaptiveSharpen )
        fi

        args+=( "$destPath" )

        echo "  Performing conversion by calling:"
        echo "    convert ${args[@]}"
        convert "${args[@]}"

        if [ ! "$keepMess" ]; then
            rm "$tmpName"
        fi

    done

done
