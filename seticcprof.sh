#!/bin/bash

ext=tif
imageDir=./
profile=

function printUsage() {
  cat << EOT
seticcprof - Recursively embed icc profile into files
-----------------------------------------------------------------------
Usage: seticcprof.sh [options] <path/to/prof> <path/to/img/or/dir>...
with possible options:
-e <str> : File extension of source images. [$ext]
Requires imagemagick to be installed and in PATH.
EOT
}

while getopts "e: ?" option; do
  case $option in
    e)  ext=${OPTARG};;
    \?) printUsage
        exit 0;;
    * ) printUsage
        exit 1;;
  esac
done

shift $((OPTIND-1))
OPTIND=1

if [[ $# -lt 2 ]]; then printUsage; exit 1; fi

profile=$1
shift

if [ ! -f "$profile" ]; then printUsage; exit 1; fi


for arg in "$@"; do

    find "$arg" -type f -name "*.$ext" -print0 | while IFS= read -r -d '' path; do

        echo Embedding profile into $path...
        mogrify +profile '*.icc,*.icm' -profile "$profile" "$path"

    done

done
