#!/bin/bash

profcheckAxes=
viewgamAxes=-n

copyright="CC BY-SA 4.0; however, no attribution is required."
descr="Input profile"
threeDFormats="h x"
manufacturer="<unknown>"
model="Input device"
name=
quality=m
refProfiles=

function printUsage() {
  cat << EOT
mkiccprof - Create device profiles from IT8 input image
-----------------------------------------------------------------------
Usage: mkiccprof.sh [options] <IT8 input image> <IT8 reference file>
with possible options:
3 <str>: Whitespace-separated list of 3D output profiles; all items
         must be either [h]tml, [w]rl or [x]3d. [$threeDFormats]
n <str>: Basename for output files. If not specified, the input
         image's basename will be used.
q <str>: Quality of device profile generation; must be one of [l]ow,
         [m]edium, [h]igh or [u]ltra. [$Quality]
r <str>: Whitespace-separated list of paths to reference profiles
         the device profiles will get compared to via 3D models. [$refProfiles]
x      : Include Lab axes in 3D output.
A <str>: Manufacturer's name. [$manufacturer]
M <str>: Device model name. [$model]
D <str>: Profile description. [$descr]
C <str>: Copyright notice.
         [$copyright]
Requires imagemagick, argyll and dispcalGUI to be installed and
in PATH.
EOT
}

function mkThreeD() {
  for outFormat in $threeDFormats; do
    if [[ "$outFormat" = "w" ]]; then
      rmWrl=0
    fi
    if [[ "$outFormat" = "h" ]]; then
      displaycal-vrml-to-x3d-converter --no-gui "$1"
    fi
    if [[ "$outFormat" = "x" ]]; then
      displaycal-vrml-to-x3d-converter --no-html --no-gui "$1"
    fi
  done
}

while getopts "3: n: q: r: x A: M: D: ?" option; do
  case $option in
    3)  threeDFormats=${OPTARG};;
    n)  name=${OPTARG};;
    q)  quality=${OPTARG};;
    r)  refProfiles=${OPTARG};;
    x)  profcheckAxes="-x";viewgamAxes="";;
    A)  manufacturer=${OPTARG};;
    M)  model=${OPTARG};;
    D)  descr=${OPTARG};;
    \?) printUsage
        exit 0;;
    * ) printUsage
        exit 1;;
  esac
done

shift $((OPTIND-1))
OPTIND=1

if [[ $# -ne 2 ]]; then printUsage; exit 1; fi

if [ ! "$name" ]; then
  name=$(basename $1)
  name="${name%.*}"
fi

# Scale down in order to recude noise and to speed up processing
convert $1 -set colorspace RGB -colorspace RGB -resize 1600x +compress it8.tif

# Find color patches and compare to set values in reference file
scanin -a -dnop -G 1.0 -v it8.tif /usr/share/color/argyll/ref/it8.cht $2

# Create shaper/matrix-based profile
#colprof -v -A "$manufacturer" -M "$model" -D "$descr" -C "$copyright" -q$quality -as -y it8
colprof -v -A "$manufacturer" -M "$model" -D "$descr" -C "$copyright" -q$quality -as it8
mv it8.icc $name-matrix.icc

# Check matrix profile and create a 3D model of the profile's gamut
profcheck -wm $profcheckAxes it8.ti3 $name-matrix.icc | tee $name-matrix.check
mkThreeD "$name-matrix.wrl"

# Calculate the matrix profile's gamut
iccgamut -w $name-matrix.icc

# Create LUT-based profile
#colprof -v -A "$manufacturer" -M "$model" -D "$descr" -C "$copyright" -q$quality -ax -y it8
colprof -v -A "$manufacturer" -M "$model" -D "$descr" -C "$copyright" -q$quality -ax it8
mv it8.icc $name-lut.icc

# Check LUT profile and create a 3D model of the profile's gamut
profcheck -wm $profcheckAxes it8.ti3 $name-lut.icc | tee $name-lut.check
mkThreeD "$name-lut.wrl"

# Calculate the LUT profile's gamut
iccgamut -w $name-lut.icc


# Create a 3D comparison of the two profiles
viewgam -w -c n $name-matrix.gam -s -t 0.5 -c n $name-lut.gam $viewgamAxes $name-matrix-lut.wrl
mkThreeD "$name-matrix-lut.wrl"

# Compare both profiles with a set of reference profiles
for refProfile in $refProfiles; do
  profileName=$(basename $refProfile)
  profileName="${profileName%.*}"
  cp $refProfile $profileName.icc
  iccgamut $profileName.icc
  viewgam -s -t 0.5 -c n $profileName.gam -w -c n $name-matrix.gam $viewgamAxes $profileName-$name-matrix.wrl
  mkThreeD "$profileName-$name-matrix.wrl"
  viewgam -s -t 0.5 -c n $profileName.gam -w -c n $name-lut.gam $viewgamAxes $profileName-$name-lut.wrl
  mkThreeD "$profileName-$name-lut.wrl"
  rm $profileName.*
done

# Cleanup a bit
mv it8.tif $name-input.tif
mv diag.tif $name-diag.tif
mv it8.ti3 $name.ti3
